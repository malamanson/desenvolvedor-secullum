# Módulo 1 - Git - Exercícios

1. Qual a diferença entre Git e GitHub/GitLab?

2. Explique a diferença entre os estados `modified` e `staged`.

3. O que é importante constar na mensagem do commit?

4. Se eu fizer um commit, ele vai aparecer no repositório remoto? Por quê? Caso negativo, o que falta?

5. Para manter um histórico mais linear e menos poluído, devemos usar `merge` ou `rebase`? Por quê?

6. Para que serve o `git stash`?

7. Envie um print de onde ficam as seguintes operações na interface gráfica do vscode:
   - `git diff`
   - `git add`
   - `git reset`
   - `git commit`
   - `git checkout` (para reverter alterações em um arquivo)
   - `git checkout` (para trocar de branch)
