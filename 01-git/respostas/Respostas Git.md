1.  Git é um sistema de controle de versões, já o Github ou Gitlab são
    locais na web ou na nuvem onde upamos e compartilhamos os projetos
    versionados pelo GIT.

2.  Modified: Modified é um arquivo que foi modificado, porém o mesmo
    ainda não foi adicionado e não está pronto para ser versionado.\
     Staged: O arquivo já foi adicionado e está pronto para ser
    versionado.Pronto para ser commitado.

3.  Na mensagem do commit é importante informar as alterações nas quais
    foram feitas no mesmo e também informações relevantes para que
    outros programadores possam se localizar no código.

4.  Não, para que o mesmo possa ser enviado ao repositório remoto, temos
    de executar o comando "push".

5.  Rebase, pois com ele os commit ficarão mais lineares e não ocorrerão
    muitas ramificações como ocorre com o "Marge".

6.  Ele guarda modificações que foram feitas e não comitadas em um
    arquivo no qual é possível ser chamado para conclusão posterior.

EX: Você está trabalhando em um Branche e precisa rapidamente commitar
uma versão, porem não gostaria de adicionar esta modificação pois ela
ainda não está concluida. Com o stash, é possível guardar essa em um
arquivo e depois seguir trabalhando nele sem que seja enviado a versão
de produção.
