# Módulo 1 - Git

## Primeiro passo

Fazer o curso grátis de Git e GitHub para iniciantes: https://www.udemy.com/course/git-e-github-para-iniciantes/

Algumas observações sobre o curso:

1. Na aula 7, ele usa comandos no terminal que só funcionam no Linux ou Mac. Para usarem os mesmos comandos no Windows, vocês devem abrir o "Git Bash" que vem junto na instalação do git. Caso queiram usar o `cmd.exe` do Windows mesmo (que é como a maioria das pessoas trabalham aqui no desenvolvimento), apenas fiquem atentos para o seguinte: ele comenta que o `cd` no Windows é `dir`, mas na verdade é `cd` mesmo. Além disso, quando ele rodar `ls -la` usem `dir /a` e quando rodar `ls` usem `dir`, que são os comandos equivalentes no `cmd` do Windows.

2. Na aula 8, ele explica como usar o vi (editor de texto). Na prática aqui na Secullum, nos usamos o Visual Studio Code como editor de texto, então sugiro que usem ele para irem se habituando. Basta baixar ele em https://code.visualstudio.com/ e rodar `git config --global core.editor "code --wait"` para configurar para o git usar ele por padrão (você vai aprender sobre esse comando de configurar editor de texto durante o curso, apenas rode ele quando chegar na aula 8). Feito isso, sempre que ele estiver usando o vi, vocês vão utilizar o vscode no lugar (que é bem mais simples).

3. Na aula 13 em diante, ele usa o GitHub. Aqui na Secullum nos usamos mais o GitLab, mas é praticamente a mesma coisa. Sugiro que ao invés do GitHub, tentem usar o GitLab para já irem se habituando no ambiente também.

4. Na aula 14, se não quiserem configurar as chaves ssh, que pode ser meio chatinho, o git vai pedir o seu usuário e senha na hora do `git push`. Essa também é a forma como a maioria das pessoas trabalham aqui, então se quiserem seguir ela, basta não configurar as chaves ssh.

## Segundo passo

Na prática, a maioria das pessoas aqui do desenvolvimento não executa todos os comando pelo terminal. Alguns comandos podem ser executados pela interface gráfica do vscode. Portanto, leiam a documentação em https://code.visualstudio.com/docs/editor/versioncontrol e procurem como executar os comandos aprendidos no curso via interface gráfica.
