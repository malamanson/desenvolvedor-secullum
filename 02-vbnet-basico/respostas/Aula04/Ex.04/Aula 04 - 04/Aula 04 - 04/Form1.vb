﻿Public Class Form1

    Private Sub Form1_load(sender As Object, e As EventArgs) Handles MyBase.Load

        Incluir_na_lista("Fiat Uno")
        Incluir_na_lista("GM Celta")
        Incluir_na_lista("VW Fusca")
        Incluir_na_lista("Ford Fiesta")

    End Sub

    Private Sub Incluir_na_lista(Nome As String)

        ComboBox1.Items.Add(Nome)
        ComboBox2.Items.Add(Nome)
        ComboBox3.Items.Add(Nome)

    End Sub

    Private Sub Excluir_Carro(Apaga As String)

        ComboBox1.Items.Remove(Apaga)
        ComboBox2.Items.Remove(Apaga)
        ComboBox3.Items.Remove(Apaga)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Excluir_Carro(TextBox1.Text)
    End Sub

End Class
