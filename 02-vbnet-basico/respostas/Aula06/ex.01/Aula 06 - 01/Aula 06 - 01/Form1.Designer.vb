﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ButtonINC = New System.Windows.Forms.Button()
        Me.ButtonEXC = New System.Windows.Forms.Button()
        Me.ButtonLIMP = New System.Windows.Forms.Button()
        Me.ButtonORD = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ButtonATU = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(47, 38)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(184, 160)
        Me.ListBox1.TabIndex = 0
        '
        'ButtonINC
        '
        Me.ButtonINC.Location = New System.Drawing.Point(296, 48)
        Me.ButtonINC.Name = "ButtonINC"
        Me.ButtonINC.Size = New System.Drawing.Size(75, 23)
        Me.ButtonINC.TabIndex = 1
        Me.ButtonINC.Text = "Incluir"
        Me.ButtonINC.UseVisualStyleBackColor = True
        '
        'ButtonEXC
        '
        Me.ButtonEXC.Location = New System.Drawing.Point(296, 90)
        Me.ButtonEXC.Name = "ButtonEXC"
        Me.ButtonEXC.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEXC.TabIndex = 2
        Me.ButtonEXC.Text = "Excluir"
        Me.ButtonEXC.UseVisualStyleBackColor = True
        '
        'ButtonLIMP
        '
        Me.ButtonLIMP.Location = New System.Drawing.Point(296, 134)
        Me.ButtonLIMP.Name = "ButtonLIMP"
        Me.ButtonLIMP.Size = New System.Drawing.Size(75, 23)
        Me.ButtonLIMP.TabIndex = 3
        Me.ButtonLIMP.Text = "Limpar"
        Me.ButtonLIMP.UseVisualStyleBackColor = True
        '
        'ButtonORD
        '
        Me.ButtonORD.Location = New System.Drawing.Point(296, 178)
        Me.ButtonORD.Name = "ButtonORD"
        Me.ButtonORD.Size = New System.Drawing.Size(75, 23)
        Me.ButtonORD.TabIndex = 4
        Me.ButtonORD.Text = "Ordenar"
        Me.ButtonORD.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(421, 115)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(167, 20)
        Me.TextBox1.TabIndex = 5
        '
        'ButtonATU
        '
        Me.ButtonATU.Location = New System.Drawing.Point(47, 216)
        Me.ButtonATU.Name = "ButtonATU"
        Me.ButtonATU.Size = New System.Drawing.Size(184, 23)
        Me.ButtonATU.TabIndex = 6
        Me.ButtonATU.Text = "Atualizar Listbox"
        Me.ButtonATU.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(628, 261)
        Me.Controls.Add(Me.ButtonATU)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.ButtonORD)
        Me.Controls.Add(Me.ButtonLIMP)
        Me.Controls.Add(Me.ButtonEXC)
        Me.Controls.Add(Me.ButtonINC)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents ButtonINC As Button
    Friend WithEvents ButtonEXC As Button
    Friend WithEvents ButtonLIMP As Button
    Friend WithEvents ButtonORD As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents ButtonATU As Button
End Class
