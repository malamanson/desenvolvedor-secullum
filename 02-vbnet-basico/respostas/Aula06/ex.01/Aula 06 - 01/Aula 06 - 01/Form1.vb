﻿Public Class Form1

    Public Listinha As New List(Of Double)

    Public Sub ButtonINC_Click(sender As Object, e As EventArgs) Handles ButtonINC.Click
        Listinha.Add(TextBox1.Text)
    End Sub

    Public Sub ButtonEXC_Click(sender As Object, e As EventArgs) Handles ButtonEXC.Click
        Listinha.RemoveAt(ListBox1.SelectedIndex)
    End Sub

    Public Sub Limpa_var(sender As Object, e As EventArgs) Handles ButtonLIMP.Click
        Listinha.Clear()
    End Sub

    Public Sub Limpar_Listbox(sender As Object, e As EventArgs) Handles ButtonLIMP.Click
        ListBox1.DataSource = Nothing
    End Sub

    Public Sub ButtonORD_Click(sender As Object, e As EventArgs) Handles ButtonORD.Click
        Listinha.Sort()
    End Sub

    Public Sub ButtonATU_Click(sender As Object, e As EventArgs) Handles ButtonATU.Click
        ListBox1.DataSource = Listinha
    End Sub
End Class
