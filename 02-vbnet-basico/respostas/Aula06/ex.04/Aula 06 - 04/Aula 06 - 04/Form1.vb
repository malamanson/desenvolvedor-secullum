﻿Public Class Form1
    Dim texto_arquivo As IO.StreamReader
    Dim linha As String

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If IO.File.Exists(TextBox1.Text) Then
                texto_arquivo = New IO.StreamReader(TextBox1.Text)
                linha = texto_arquivo.ReadToEnd

                While linha <> Nothing
                    TextBox2.Text = linha
                    linha = texto_arquivo.ReadToEnd
                End While
                texto_arquivo.Close()
                texto_arquivo.Dispose()
            Else
                MessageBox.Show("Escreva o caminho do arquivo!!!")
            End If
        Catch ex As Exception
            MessageBox.Show("Houve um erro ao abrir arquivo!!!")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Try
            Dim salvarComo As SaveFileDialog = New SaveFileDialog()
            Dim texto_arquivo As New IO.StreamWriter(TextBox1.Text)

            texto_arquivo.Write(TextBox2.Text)
            texto_arquivo.Close()
            texto_arquivo.Dispose()
        Catch ex As Exception
            MessageBox.Show("Houve um erro ao salvar o arquivo")
        End Try
    End Sub
End Class
