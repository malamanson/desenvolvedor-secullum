﻿Public Class Form1

    Dim Numeros As New List(Of Integer)

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Numeros.Add(TextBox1.Text)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Numeros.RemoveAt(ListBox1.SelectedIndex)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Numeros.Clear()
    End Sub

    Private Sub LimparListbox(sender As Object, e As EventArgs) Handles Button3.Click
        ListBox1.DataSource = Nothing
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ListBox1.DataSource = Numeros
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim ResultadoLinQ = From Elemento In Numeros Where Elemento > TextBox2.Text Order By Elemento Descending

        For Each Total In ResultadoLinQ
            ListBox2.Items.Add(Total)
        Next
    End Sub

End Class
