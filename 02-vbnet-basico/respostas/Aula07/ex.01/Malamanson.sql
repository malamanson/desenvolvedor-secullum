USE [master]
GO

CREATE DATABASE Malamanson;
GO

USE [Malamanson]
GO
/****** Object:  Table [dbo].[pessoas]    Script Date: 03/02/2020 01:13:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pessoas](
	[Nome] [nchar](50) NULL,
	[Endereco] [nchar](100) NULL,
	[Nascimento] [date] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Cidade] [nchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cidades]    Script Date: 03/02/2020 01:13:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cidades](
	[Nome] [varchar](50) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
