﻿Imports Aula_07___Negocio


Public Class Form1
    Private Sub ButtonIncluir_Click(sender As Object, e As EventArgs) Handles ButtonIncluir.Click
        Dim _cidade As New Cidade
        _cidade.InserirCidade(TextBox1.Text)
    End Sub

    Private Sub ButtonListar_Click(sender As Object, e As EventArgs) Handles ButtonListar.Click
        Dim _cidade As New Cidade
        DataGridView1.DataSource = _cidade.RetornaTabela
    End Sub

    Private Sub ButtonExcluir_Click(sender As Object, e As EventArgs) Handles ButtonExcluir.Click
        Dim _cidade As New Cidade
        Try
            _cidade.DeletaCidade(DataGridView1.CurrentRow.Cells(1).Value)
        Catch ex As Exception
            MsgBox("Selecione uma cidade!")
        End Try
    End Sub

    Private Sub ButtonAlterar_Click(sender As Object, e As EventArgs) Handles ButtonAlterar.Click
        Dim _cidade As New Cidade
        _cidade.AlteraCidade(TextBox1.Text, DataGridView1.CurrentRow.Cells(1).Value)
    End Sub


End Class
