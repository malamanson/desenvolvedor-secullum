﻿Imports Aula_07___Negocio

Public Class Form2

    Public Sub Form2_Load(ByVal sender As Object,
    ByVal e As System.EventArgs) Handles MyBase.Load
        Dim _cidade As New Cidade
        ComboBox1.DataSource = _cidade.RetornaTabela
        ComboBox1.DisplayMember = "nome"
        ComboBox1.ValueMember = "nome"
    End Sub

    Private Sub ButtonList_Click(sender As Object, e As EventArgs) Handles ButtonList.Click
        Dim _pessoa As New Pessoa

        DataGridView1.DataSource = _pessoa.ListarPessoas

    End Sub

    Private Sub ButtonInser_Click(sender As Object, e As EventArgs) Handles ButtonInser.Click
        Dim _pessoa As New Pessoa

        _pessoa.Inserir_Pessoa(TextBox1.Text, ComboBox1.SelectedValue, TextBox2.Text, TextBox3.Text)

    End Sub

    Private Sub ButtonRem_Click(sender As Object, e As EventArgs) Handles ButtonRem.Click
        Dim _pessoa As New Pessoa
        Try
            _pessoa.DeletaPessoa(DataGridView1.CurrentRow.Cells(3).Value)

        Catch ex As Exception
            MsgBox("Selecione uma Pessoa!")

        End Try
    End Sub

    Private Sub ButtonAlt_Click(sender As Object, e As EventArgs) Handles ButtonAlt.Click

        Dim _pessoa As New Pessoa
        _pessoa.Alterapessoa(TextBox1.Text, ComboBox1.SelectedValue, TextBox2.Text, TextBox3.Text, DataGridView1.CurrentRow.Cells(3).Value.ToString)
    End Sub
End Class

