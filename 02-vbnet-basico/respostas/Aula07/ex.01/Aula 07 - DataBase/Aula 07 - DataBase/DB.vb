﻿Imports System.Data.SqlClient

Public Class DB

    Public Const String_Conexao = "Data Source=.\sqlexpress;Initial Catalog=Malamanson;Integrated Security=True"

    Public Function Executa_Comando(ComandoSQL As String) As Boolean

        Try

            Using conexao As New SqlConnection

                conexao.ConnectionString = String_Conexao
                conexao.Open()

                Using comando As New SqlCommand

                    comando.Connection = conexao
                    comando.CommandText = ComandoSQL
                    comando.ExecuteNonQuery()


                End Using
                Return True
            End Using



        Catch ex As Exception
            Return False

        End Try

    End Function

    Public Function Abre_Tabela(ComandoSQL As String) As DataTable
        Dim tabela As New DataTable
        Try


            Using conexao As New SqlConnection
                conexao.ConnectionString = String_Conexao
                conexao.Open()

                Using comando As New SqlCommand
                    comando.Connection = conexao
                    comando.CommandText = ComandoSQL

                    Using leitor = comando.ExecuteReader
                        tabela.Load(leitor)

                    End Using

                End Using


            End Using


        Catch ex As Exception

        End Try

        Return tabela

    End Function

End Class