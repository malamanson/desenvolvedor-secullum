﻿Imports System.Net.Sockets
Imports System.Text
Imports System.Threading

Public Class Form1
    Dim ClasseOnline As New ClasseOnline
    Dim ClasseOffline As New ClasseOffline

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Label1.Text = ClasseOffline.MensagemPD("Malamanson")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Label1.Text = ClasseOffline.EnviaDataHora()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Label1.Text = ClasseOffline.EnviaLista()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim Retorno = ""
        While Retorno <> "REGISTRO_OFFLINE|VAZIO"
            Retorno = ClasseOffline.ReceberRegistros
            ListBox1.Items.Add(Retorno)
        End While
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim thread As New Thread(AddressOf ClasseOnline.LigarOnline)
        If thread.ThreadState = ThreadState.Unstarted = True Then
            thread.Start()
        ElseIf thread.ThreadState = ThreadState.Stopped = True Then
            thread = New Thread(AddressOf ClasseOnline.LigarOnline)
            thread.Start()
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        ClasseOnline.DesligarOnline()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
