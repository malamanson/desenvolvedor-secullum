﻿Imports System.Net.Sockets
Imports System.Text
Imports System.Threading

Public Class ClasseOnline
    Inherits ClasseOffline
    Public Shared online As Boolean

    Public Sub LigarOnline()
        Try
            Dim matriculas = ListaMatricula()
            ConectarEquipamento("LocalHost", 3040)
            online = True
            Using ns2 As NetworkStream = _cliente.GetStream
                Dim dadosComunicacao As String
                While (online)
                    Dim rbuf(10000) As Byte
                    dadosComunicacao = ns2.Read(rbuf, 0, rbuf.Length)
                    Thread.Sleep(200)
                    If (dadosComunicacao <> Nothing) And (dadosComunicacao <> 0) Then
                        dadosComunicacao = (Encoding.ASCII.GetString(rbuf))
                        If (dadosComunicacao.Contains("INI|OK|FIM")) Then
                        Else
                            Dim liberado As Boolean = 0
                            dadosComunicacao = ExtrairMatricula(dadosComunicacao)
                            For Each item In matriculas
                                If item = dadosComunicacao Then
                                    Dim wbuf = Encoding.ASCII.GetBytes("INI|RESPOSTA|1|3|acesso liberado|FIM")
                                    ns2.Write(wbuf, 0, wbuf.Length)
                                    liberado = 1
                                    dadosComunicacao = Nothing
                                End If
                            Next
                            If liberado = 0 Then
                                Dim wbuf = Encoding.ASCII.GetBytes("INI|RESPOSTA|0|3|acesso negado|FIM")
                                ns2.Write(wbuf, 0, wbuf.Length)
                                dadosComunicacao = Nothing
                            End If
                        End If
                    End If
                End While
                Exit Sub
            End Using
        Catch
            Exit Sub
        End Try
    End Sub

    Public Sub DesligarOnline()
        Try
            online = False
            _cliente.GetStream().Close()
        Catch
            Thread.Sleep(300)
        End Try
    End Sub
End Class
