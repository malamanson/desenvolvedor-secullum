﻿Imports System.Net.Sockets
Imports System.Text
Imports System.Threading

Public Class ClasseOffline
    Public Shared _cliente As New TcpClient

    Function ListaMatricula()
        Dim ListaMatriculas As New List(Of Integer)
        ListaMatriculas.Add(12)
        ListaMatriculas.Add(16)
        ListaMatriculas.Add(20)
        ListaMatriculas.Add(24)
        ListaMatriculas.Add(28)
        ListaMatriculas.Add(32)
        ListaMatriculas.Add(36)
        ListaMatriculas.Add(40)
        ListaMatriculas.Add(44)
        ListaMatriculas.Add(48)
        ListaMatriculas.Add(52)
        ListaMatriculas.Add(56)
        ListaMatriculas.Add(666)
        Return ListaMatriculas
    End Function

    Public Sub ConectarEquipamento(endereco As String, porta As Integer)
        Try
            If Not _cliente.Connected Then
                _cliente = New TcpClient
                _cliente.Connect(endereco, porta)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function ExtrairMatricula(dados As String)
        Dim Retorno = dados
        Retorno = Retorno.Replace("INI|", "")
        Retorno = Retorno.Replace("|FIM", "")
        Retorno = Retorno.Replace("|ONLINE", "")
        Retorno = Retorno.Replace("ONLINE|", "")
        Retorno = Retorno.Replace("|OFFLINE", "")
        Retorno = Retorno.Replace(vbNullChar, "")
        Retorno = Retorno.TrimStart("0"c)
        Return Retorno
    End Function

    Private Function EnviaDadosEquipamento(dados As String)
        Try
            Dim classeOnline As New ClasseOnline
            Dim Reconectar As Boolean
            If ClasseOnline.online Then
                classeOnline.DesligarOnline()
                Reconectar = True
            End If
            ConectarEquipamento("LocalHost", 3040)
            Dim Retorno As String
            Using _ns As NetworkStream = _cliente.GetStream

                Dim MPE = Encoding.ASCII.GetBytes(dados)
                _ns.Write(MPE, 0, MPE.Length)
                Thread.Sleep(500)
                Dim rbuf(10000) As Byte
                _ns.Read(rbuf, 0, rbuf.Length)
                Retorno = Encoding.ASCII.GetString(rbuf)
                Retorno = Retorno.Replace(vbNullChar, "")
                Retorno = Retorno.Replace("INI|", "")
                Retorno = Retorno.Replace("|FIM", "")
                _cliente.Close()
                If Reconectar Then
                    Dim thread As New Thread(AddressOf classeOnline.LigarOnline)
                    thread = New Thread(AddressOf classeOnline.LigarOnline)
                    thread.Start()
                End If
                Return Retorno
            End Using

        Catch ex As Exception
            Return "Erro"
        End Try
    End Function


    Public Function MensagemPD(texto As String)
        Dim RetornoEquipamento As String
        Try
            RetornoEquipamento = EnviaDadosEquipamento("INI|MENSAGEM_PADRAO|Malamanson|FIM")
            If RetornoEquipamento = "OK" Then
                MsgBox("Mensagem padrão enviada com sucesso!")
            End If
            Return RetornoEquipamento

        Catch ex As Exception
            MsgBox("Erro ao enviar dados")
            Return "Erro"
        End Try
        Return RetornoEquipamento

    End Function

    Public Function EnviaDataHora()
        Dim RetornoEquipamento As String
        Try
            RetornoEquipamento = EnviaDadosEquipamento("INI|DATA_HORA|06/06/2006 09:42:23|FIM")
            If RetornoEquipamento = "OK" Then
                MsgBox("Data e Hora Enviados com êxito")
            End If
            Return RetornoEquipamento

        Catch ex As Exception
            MsgBox("Erro ao enviar dados")
            Return "Erro"
        End Try
        Return RetornoEquipamento

    End Function

    Public Function EnviaLista()
        Dim RetornoEquipamento As String
        Try
            Dim stringEnvio As String = "INI|LISTA|"
            Dim matriculas As List(Of Integer) = ListaMatricula()
            Dim stringMatriculas As String = Nothing
            For Each item In matriculas
                stringMatriculas = stringMatriculas + item.ToString + "|"
            Next
            stringEnvio = stringEnvio + stringMatriculas + "FIM"
            RetornoEquipamento = EnviaDadosEquipamento(stringEnvio)
            If RetornoEquipamento = "OK" Then
                MsgBox("Lista Enviada com Sucesso!")
            End If
            Return RetornoEquipamento

        Catch ex As Exception
            MsgBox("Erro ao enviar dados")
            Return "Erro"
        End Try
        Return RetornoEquipamento

    End Function

    Public Function ReceberRegistros()
        Dim RetornoEquipamento As String
        Try
            RetornoEquipamento = EnviaDadosEquipamento("INI|REGISTRO_OFFLINE|FIM")
            Return RetornoEquipamento

        Catch ex As Exception
            MsgBox("Erro ao enviar dados")
            Return "Erro"
        End Try
        Return RetornoEquipamento

    End Function

End Class
