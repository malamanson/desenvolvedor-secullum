﻿Public Class Inteiro
    ''' <summary>
    ''' Variavel Inteiro solicitada no enunciado do exercicio
    ''' </summary>
    Public NumeroTeste As Integer


    ''' <summary>
    ''' Sub na qual informamos o numero inteiro
    ''' </summary>
    Public Sub InformaInteiro(Numero As String)
        NumeroTeste = Numero
    End Sub


    ''' <summary>
    ''' Function que verifica se o valor digitado no FORM1 é par
    ''' </summary>
    ''' <returns>True ou False</returns>
    Public Function Ehpar() As Boolean
        If NumeroTeste Mod 2 = 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function Ehimpar() As Boolean
        If NumeroTeste Mod 2 <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function RetornaIntero() As String
        Return Me.NumeroTeste
    End Function

End Class
