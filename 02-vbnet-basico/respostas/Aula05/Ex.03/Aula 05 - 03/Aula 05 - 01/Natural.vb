﻿Imports Aula_05___01

Public Class Natural

    Inherits Inteiro

    Implements INumero

    Public Property Valor As String Implements INumero.Valor
        Get
            Return Me.NumeroTeste
        End Get
        Set(value As String)
            NumeroTeste = Valor
        End Set
    End Property

    Public Function EhNatural() As Boolean
        If NumeroTeste > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function EhZero() As Boolean Implements INumero.EhZero
        If NumeroTeste = 0 Then
            Return True
        Else
            Return False

        End If
    End Function
End Class
