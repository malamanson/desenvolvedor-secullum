﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBoxBaseTri = New System.Windows.Forms.TextBox()
        Me.TextBoxBaRe = New System.Windows.Forms.TextBox()
        Me.TextBoxAltTri = New System.Windows.Forms.TextBox()
        Me.TextBoxAlRet = New System.Windows.Forms.TextBox()
        Me.BaseLabel1 = New System.Windows.Forms.Label()
        Me.LabelResult1 = New System.Windows.Forms.Label()
        Me.AreaLabel2 = New System.Windows.Forms.Label()
        Me.AreaLabel1 = New System.Windows.Forms.Label()
        Me.LabelResult2 = New System.Windows.Forms.Label()
        Me.BaseLabel2 = New System.Windows.Forms.Label()
        Me.AlturaLabel2 = New System.Windows.Forms.Label()
        Me.AlturaLabel1 = New System.Windows.Forms.Label()
        Me.Label1Nome = New System.Windows.Forms.Label()
        Me.Label2Nome = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(487, 66)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 50)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(487, 163)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 51)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBoxBaseTri
        '
        Me.TextBoxBaseTri.Location = New System.Drawing.Point(128, 179)
        Me.TextBoxBaseTri.Name = "TextBoxBaseTri"
        Me.TextBoxBaseTri.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxBaseTri.TabIndex = 3
        '
        'TextBoxBaRe
        '
        Me.TextBoxBaRe.Location = New System.Drawing.Point(128, 85)
        Me.TextBoxBaRe.Name = "TextBoxBaRe"
        Me.TextBoxBaRe.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxBaRe.TabIndex = 4
        '
        'TextBoxAltTri
        '
        Me.TextBoxAltTri.Location = New System.Drawing.Point(309, 179)
        Me.TextBoxAltTri.Name = "TextBoxAltTri"
        Me.TextBoxAltTri.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAltTri.TabIndex = 5
        '
        'TextBoxAlRet
        '
        Me.TextBoxAlRet.Location = New System.Drawing.Point(309, 85)
        Me.TextBoxAlRet.Name = "TextBoxAlRet"
        Me.TextBoxAlRet.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAlRet.TabIndex = 6
        '
        'BaseLabel1
        '
        Me.BaseLabel1.AutoSize = True
        Me.BaseLabel1.Location = New System.Drawing.Point(45, 88)
        Me.BaseLabel1.Name = "BaseLabel1"
        Me.BaseLabel1.Size = New System.Drawing.Size(31, 13)
        Me.BaseLabel1.TabIndex = 7
        Me.BaseLabel1.Text = "Base"
        '
        'LabelResult1
        '
        Me.LabelResult1.AutoSize = True
        Me.LabelResult1.Location = New System.Drawing.Point(776, 85)
        Me.LabelResult1.Name = "LabelResult1"
        Me.LabelResult1.Size = New System.Drawing.Size(13, 13)
        Me.LabelResult1.TabIndex = 8
        Me.LabelResult1.Text = "0"
        '
        'AreaLabel2
        '
        Me.AreaLabel2.AutoSize = True
        Me.AreaLabel2.Location = New System.Drawing.Point(677, 182)
        Me.AreaLabel2.Name = "AreaLabel2"
        Me.AreaLabel2.Size = New System.Drawing.Size(29, 13)
        Me.AreaLabel2.TabIndex = 9
        Me.AreaLabel2.Text = "Área"
        '
        'AreaLabel1
        '
        Me.AreaLabel1.AutoSize = True
        Me.AreaLabel1.Location = New System.Drawing.Point(677, 85)
        Me.AreaLabel1.Name = "AreaLabel1"
        Me.AreaLabel1.Size = New System.Drawing.Size(29, 13)
        Me.AreaLabel1.TabIndex = 10
        Me.AreaLabel1.Text = "Área"
        '
        'LabelResult2
        '
        Me.LabelResult2.AutoSize = True
        Me.LabelResult2.Location = New System.Drawing.Point(776, 182)
        Me.LabelResult2.Name = "LabelResult2"
        Me.LabelResult2.Size = New System.Drawing.Size(13, 13)
        Me.LabelResult2.TabIndex = 11
        Me.LabelResult2.Text = "0"
        '
        'BaseLabel2
        '
        Me.BaseLabel2.AutoSize = True
        Me.BaseLabel2.Location = New System.Drawing.Point(45, 182)
        Me.BaseLabel2.Name = "BaseLabel2"
        Me.BaseLabel2.Size = New System.Drawing.Size(31, 13)
        Me.BaseLabel2.TabIndex = 12
        Me.BaseLabel2.Text = "Base"
        '
        'AlturaLabel2
        '
        Me.AlturaLabel2.AutoSize = True
        Me.AlturaLabel2.Location = New System.Drawing.Point(250, 179)
        Me.AlturaLabel2.Name = "AlturaLabel2"
        Me.AlturaLabel2.Size = New System.Drawing.Size(34, 13)
        Me.AlturaLabel2.TabIndex = 13
        Me.AlturaLabel2.Text = "Altura"
        '
        'AlturaLabel1
        '
        Me.AlturaLabel1.AutoSize = True
        Me.AlturaLabel1.Location = New System.Drawing.Point(250, 88)
        Me.AlturaLabel1.Name = "AlturaLabel1"
        Me.AlturaLabel1.Size = New System.Drawing.Size(34, 13)
        Me.AlturaLabel1.TabIndex = 14
        Me.AlturaLabel1.Text = "Altura"
        '
        'Label1Nome
        '
        Me.Label1Nome.AutoSize = True
        Me.Label1Nome.Location = New System.Drawing.Point(45, 44)
        Me.Label1Nome.Name = "Label1Nome"
        Me.Label1Nome.Size = New System.Drawing.Size(59, 13)
        Me.Label1Nome.TabIndex = 15
        Me.Label1Nome.Text = "Retângulo:"
        '
        'Label2Nome
        '
        Me.Label2Nome.AutoSize = True
        Me.Label2Nome.Location = New System.Drawing.Point(45, 143)
        Me.Label2Nome.Name = "Label2Nome"
        Me.Label2Nome.Size = New System.Drawing.Size(54, 13)
        Me.Label2Nome.TabIndex = 16
        Me.Label2Nome.Text = "Triângulo:"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(873, 253)
        Me.Controls.Add(Me.Label2Nome)
        Me.Controls.Add(Me.Label1Nome)
        Me.Controls.Add(Me.AlturaLabel1)
        Me.Controls.Add(Me.AlturaLabel2)
        Me.Controls.Add(Me.BaseLabel2)
        Me.Controls.Add(Me.LabelResult2)
        Me.Controls.Add(Me.AreaLabel1)
        Me.Controls.Add(Me.AreaLabel2)
        Me.Controls.Add(Me.LabelResult1)
        Me.Controls.Add(Me.BaseLabel1)
        Me.Controls.Add(Me.TextBoxAlRet)
        Me.Controls.Add(Me.TextBoxAltTri)
        Me.Controls.Add(Me.TextBoxBaRe)
        Me.Controls.Add(Me.TextBoxBaseTri)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents TextBoxBaseTri As TextBox
    Friend WithEvents TextBoxBaRe As TextBox
    Friend WithEvents TextBoxAltTri As TextBox
    Friend WithEvents TextBoxAlRet As TextBox
    Friend WithEvents BaseLabel1 As Label
    Friend WithEvents LabelResult1 As Label
    Friend WithEvents AreaLabel2 As Label
    Friend WithEvents AreaLabel1 As Label
    Friend WithEvents LabelResult2 As Label
    Friend WithEvents BaseLabel2 As Label
    Friend WithEvents AlturaLabel2 As Label
    Friend WithEvents AlturaLabel1 As Label
    Friend WithEvents Label1Nome As Label
    Friend WithEvents Label2Nome As Label
End Class
