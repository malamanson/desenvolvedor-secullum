﻿Public Class Form1
    Public AreaRetangulo As New Retangulo
    Public AreaTriangulo As New Triangulo

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        AreaRetangulo.BasePP = TextBoxBaRe.Text
        AreaRetangulo.AltPP = TextBoxAlRet.Text
        LabelResult1.Text = AreaRetangulo.RetornaArea
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        AreaTriangulo.BasePP = TextBoxBaseTri.Text
        AreaTriangulo.AltPP = TextBoxAltTri.Text
        LabelResult2.Text = AreaTriangulo.RetornaAreaTri

    End Sub
End Class
