﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LinkLabelAtu = New System.Windows.Forms.LinkLabel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxIncClas = New System.Windows.Forms.TextBox()
        Me.ButtonIncClas = New System.Windows.Forms.Button()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxIdAltClass = New System.Windows.Forms.TextBox()
        Me.TextBoxAltClas = New System.Windows.Forms.TextBox()
        Me.ButtonAltClas = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxExcClas = New System.Windows.Forms.TextBox()
        Me.ButtonExcClas = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabControl3 = New System.Windows.Forms.TabControl()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ComboBoxClassPess = New System.Windows.Forms.ComboBox()
        Me.TextBoxNomePess = New System.Windows.Forms.TextBox()
        Me.TextBoxEmailPess = New System.Windows.Forms.TextBox()
        Me.ButtonIncPess = New System.Windows.Forms.Button()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBoxIdAlt = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ComboBoxAltClass = New System.Windows.Forms.ComboBox()
        Me.TextBoxEmailAltPess = New System.Windows.Forms.TextBox()
        Me.TextBoxNomAltPess = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.LinkLabelAtuII = New System.Windows.Forms.LinkLabel()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBoxNomeExcPess = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabControl3.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage8.SuspendLayout()
        Me.SuspendLayout()
        '
        'LinkLabelAtu
        '
        Me.LinkLabelAtu.AutoSize = True
        Me.LinkLabelAtu.Location = New System.Drawing.Point(15, 12)
        Me.LinkLabelAtu.Name = "LinkLabelAtu"
        Me.LinkLabelAtu.Size = New System.Drawing.Size(50, 13)
        Me.LinkLabelAtu.TabIndex = 0
        Me.LinkLabelAtu.TabStop = True
        Me.LinkLabelAtu.Text = "Atualizar:"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(658, 386)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Controls.Add(Me.TabControl2)
        Me.TabPage1.Controls.Add(Me.LinkLabelAtu)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(650, 360)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Classificação"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(18, 41)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(613, 180)
        Me.DataGridView1.TabIndex = 2
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Location = New System.Drawing.Point(18, 227)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(613, 127)
        Me.TabControl2.TabIndex = 1
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label1)
        Me.TabPage3.Controls.Add(Me.TextBoxIncClas)
        Me.TabPage3.Controls.Add(Me.ButtonIncClas)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(605, 101)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "Incluir"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Descrição:"
        '
        'TextBoxIncClas
        '
        Me.TextBoxIncClas.Location = New System.Drawing.Point(96, 22)
        Me.TextBoxIncClas.Name = "TextBoxIncClas"
        Me.TextBoxIncClas.Size = New System.Drawing.Size(235, 20)
        Me.TextBoxIncClas.TabIndex = 3
        '
        'ButtonIncClas
        '
        Me.ButtonIncClas.Location = New System.Drawing.Point(96, 48)
        Me.ButtonIncClas.Name = "ButtonIncClas"
        Me.ButtonIncClas.Size = New System.Drawing.Size(75, 23)
        Me.ButtonIncClas.TabIndex = 3
        Me.ButtonIncClas.Text = "Incluir"
        Me.ButtonIncClas.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Label4)
        Me.TabPage4.Controls.Add(Me.TextBoxIdAltClass)
        Me.TabPage4.Controls.Add(Me.TextBoxAltClas)
        Me.TabPage4.Controls.Add(Me.ButtonAltClas)
        Me.TabPage4.Controls.Add(Me.Label2)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(605, 101)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Alterar"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(218, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(21, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "ID:"
        '
        'TextBoxIdAltClass
        '
        Me.TextBoxIdAltClass.Location = New System.Drawing.Point(245, 53)
        Me.TextBoxIdAltClass.Name = "TextBoxIdAltClass"
        Me.TextBoxIdAltClass.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxIdAltClass.TabIndex = 3
        '
        'TextBoxAltClas
        '
        Me.TextBoxAltClas.Location = New System.Drawing.Point(90, 24)
        Me.TextBoxAltClas.Name = "TextBoxAltClas"
        Me.TextBoxAltClas.Size = New System.Drawing.Size(255, 20)
        Me.TextBoxAltClas.TabIndex = 2
        '
        'ButtonAltClas
        '
        Me.ButtonAltClas.Location = New System.Drawing.Point(90, 50)
        Me.ButtonAltClas.Name = "ButtonAltClas"
        Me.ButtonAltClas.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAltClas.TabIndex = 1
        Me.ButtonAltClas.Text = "Alterar"
        Me.ButtonAltClas.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Descrição:"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label3)
        Me.TabPage5.Controls.Add(Me.TextBoxExcClas)
        Me.TabPage5.Controls.Add(Me.ButtonExcClas)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(605, 101)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "Excluir"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(74, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(21, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "ID:"
        '
        'TextBoxExcClas
        '
        Me.TextBoxExcClas.Location = New System.Drawing.Point(101, 27)
        Me.TextBoxExcClas.Name = "TextBoxExcClas"
        Me.TextBoxExcClas.Size = New System.Drawing.Size(273, 20)
        Me.TextBoxExcClas.TabIndex = 1
        '
        'ButtonExcClas
        '
        Me.ButtonExcClas.Location = New System.Drawing.Point(101, 53)
        Me.ButtonExcClas.Name = "ButtonExcClas"
        Me.ButtonExcClas.Size = New System.Drawing.Size(75, 23)
        Me.ButtonExcClas.TabIndex = 0
        Me.ButtonExcClas.Text = "Excluir"
        Me.ButtonExcClas.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TabControl3)
        Me.TabPage2.Controls.Add(Me.LinkLabelAtuII)
        Me.TabPage2.Controls.Add(Me.DataGridView2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(650, 360)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Pessoas"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabControl3
        '
        Me.TabControl3.Controls.Add(Me.TabPage6)
        Me.TabControl3.Controls.Add(Me.TabPage7)
        Me.TabControl3.Controls.Add(Me.TabPage8)
        Me.TabControl3.Location = New System.Drawing.Point(15, 209)
        Me.TabControl3.Name = "TabControl3"
        Me.TabControl3.SelectedIndex = 0
        Me.TabControl3.Size = New System.Drawing.Size(608, 145)
        Me.TabControl3.TabIndex = 2
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Label11)
        Me.TabPage6.Controls.Add(Me.Label12)
        Me.TabPage6.Controls.Add(Me.Label13)
        Me.TabPage6.Controls.Add(Me.ComboBoxClassPess)
        Me.TabPage6.Controls.Add(Me.TextBoxNomePess)
        Me.TabPage6.Controls.Add(Me.TextBoxEmailPess)
        Me.TabPage6.Controls.Add(Me.ButtonIncPess)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(600, 119)
        Me.TabPage6.TabIndex = 0
        Me.TabPage6.Text = "Incluir"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(27, 60)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 13)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "Classificação:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(61, 35)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "E-mail:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(61, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(38, 13)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "Nome:"
        '
        'ComboBoxClassPess
        '
        Me.ComboBoxClassPess.FormattingEnabled = True
        Me.ComboBoxClassPess.Location = New System.Drawing.Point(103, 57)
        Me.ComboBoxClassPess.Name = "ComboBoxClassPess"
        Me.ComboBoxClassPess.Size = New System.Drawing.Size(269, 21)
        Me.ComboBoxClassPess.TabIndex = 5
        '
        'TextBoxNomePess
        '
        Me.TextBoxNomePess.Location = New System.Drawing.Point(103, 6)
        Me.TextBoxNomePess.Name = "TextBoxNomePess"
        Me.TextBoxNomePess.Size = New System.Drawing.Size(269, 20)
        Me.TextBoxNomePess.TabIndex = 3
        '
        'TextBoxEmailPess
        '
        Me.TextBoxEmailPess.Location = New System.Drawing.Point(103, 32)
        Me.TextBoxEmailPess.Name = "TextBoxEmailPess"
        Me.TextBoxEmailPess.Size = New System.Drawing.Size(269, 20)
        Me.TextBoxEmailPess.TabIndex = 4
        '
        'ButtonIncPess
        '
        Me.ButtonIncPess.Location = New System.Drawing.Point(102, 85)
        Me.ButtonIncPess.Name = "ButtonIncPess"
        Me.ButtonIncPess.Size = New System.Drawing.Size(75, 23)
        Me.ButtonIncPess.TabIndex = 0
        Me.ButtonIncPess.Text = "Incluir"
        Me.ButtonIncPess.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.Label14)
        Me.TabPage7.Controls.Add(Me.TextBoxIdAlt)
        Me.TabPage7.Controls.Add(Me.Label8)
        Me.TabPage7.Controls.Add(Me.Label9)
        Me.TabPage7.Controls.Add(Me.Label10)
        Me.TabPage7.Controls.Add(Me.ComboBoxAltClass)
        Me.TabPage7.Controls.Add(Me.TextBoxEmailAltPess)
        Me.TabPage7.Controls.Add(Me.TextBoxNomAltPess)
        Me.TabPage7.Controls.Add(Me.Button1)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(600, 119)
        Me.TabPage7.TabIndex = 1
        Me.TabPage7.Text = "Alterar"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(390, 9)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 13)
        Me.Label14.TabIndex = 11
        Me.Label14.Text = "Id:"
        '
        'TextBoxIdAlt
        '
        Me.TextBoxIdAlt.Location = New System.Drawing.Point(415, 6)
        Me.TextBoxIdAlt.Name = "TextBoxIdAlt"
        Me.TextBoxIdAlt.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxIdAlt.TabIndex = 10
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 61)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Classificação:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(48, 35)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(38, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "E-mail:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(48, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Nome:"
        '
        'ComboBoxAltClass
        '
        Me.ComboBoxAltClass.FormattingEnabled = True
        Me.ComboBoxAltClass.Location = New System.Drawing.Point(92, 58)
        Me.ComboBoxAltClass.Name = "ComboBoxAltClass"
        Me.ComboBoxAltClass.Size = New System.Drawing.Size(155, 21)
        Me.ComboBoxAltClass.TabIndex = 3
        '
        'TextBoxEmailAltPess
        '
        Me.TextBoxEmailAltPess.Location = New System.Drawing.Point(92, 32)
        Me.TextBoxEmailAltPess.Name = "TextBoxEmailAltPess"
        Me.TextBoxEmailAltPess.Size = New System.Drawing.Size(266, 20)
        Me.TextBoxEmailAltPess.TabIndex = 2
        '
        'TextBoxNomAltPess
        '
        Me.TextBoxNomAltPess.Location = New System.Drawing.Point(92, 6)
        Me.TextBoxNomAltPess.Name = "TextBoxNomAltPess"
        Me.TextBoxNomAltPess.Size = New System.Drawing.Size(266, 20)
        Me.TextBoxNomAltPess.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(92, 85)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Alterar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'LinkLabelAtuII
        '
        Me.LinkLabelAtuII.AutoSize = True
        Me.LinkLabelAtuII.Location = New System.Drawing.Point(22, 17)
        Me.LinkLabelAtuII.Name = "LinkLabelAtuII"
        Me.LinkLabelAtuII.Size = New System.Drawing.Size(50, 13)
        Me.LinkLabelAtuII.TabIndex = 1
        Me.LinkLabelAtuII.TabStop = True
        Me.LinkLabelAtuII.Text = "Atualizar:"
        '
        'DataGridView2
        '
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(15, 46)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(608, 157)
        Me.DataGridView2.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(90, 64)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "Excluir"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBoxNomeExcPess
        '
        Me.TextBoxNomeExcPess.Location = New System.Drawing.Point(99, 26)
        Me.TextBoxNomeExcPess.Name = "TextBoxNomeExcPess"
        Me.TextBoxNomeExcPess.Size = New System.Drawing.Size(266, 20)
        Me.TextBoxNomeExcPess.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(72, 29)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(21, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "ID:"
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.Label5)
        Me.TabPage8.Controls.Add(Me.TextBoxNomeExcPess)
        Me.TabPage8.Controls.Add(Me.Button2)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(600, 119)
        Me.TabPage8.TabIndex = 2
        Me.TabPage8.Text = "Excluir"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 410)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabControl3.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage8.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LinkLabelAtu As LinkLabel
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents LinkLabelAtuII As LinkLabel
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents TabControl3 As TabControl
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxIncClas As TextBox
    Friend WithEvents ButtonIncClas As Button
    Friend WithEvents TextBoxAltClas As TextBox
    Friend WithEvents ButtonAltClas As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxExcClas As TextBox
    Friend WithEvents ButtonExcClas As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBoxIdAltClass As TextBox
    Friend WithEvents TextBoxNomePess As TextBox
    Friend WithEvents TextBoxEmailPess As TextBox
    Friend WithEvents ButtonIncPess As Button
    Friend WithEvents ComboBoxClassPess As ComboBox
    Friend WithEvents ComboBoxAltClass As ComboBox
    Friend WithEvents TextBoxEmailAltPess As TextBox
    Friend WithEvents TextBoxNomAltPess As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents TextBoxIdAlt As TextBox
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents Label5 As Label
    Friend WithEvents TextBoxNomeExcPess As TextBox
    Friend WithEvents Button2 As Button
End Class
