﻿
Public Class Form1

    Public Sub Form2_Load(ByVal sender As Object,
    ByVal e As System.EventArgs) Handles MyBase.Load
        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient
        ComboBoxClassPess.DataSource = ws.ClassificacoesListar
        ComboBoxClassPess.DisplayMember = "Descricao"
        ComboBoxClassPess.ValueMember = "id"

        ComboBoxAltClass.DataSource = ws.ClassificacoesListar
        ComboBoxAltClass.DisplayMember = "Descricao"
        ComboBoxAltClass.ValueMember = "id"

    End Sub

    Private Sub LinkLabelAtu_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabelAtu.LinkClicked

        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient

        DataGridView1.DataSource = Nothing
        DataGridView1.DataSource = ws.ClassificacoesListar

    End Sub

    Private Sub ButtonIncClas_Click(sender As Object, e As EventArgs) Handles ButtonIncClas.Click
        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient
        If ws.ClassificacoesIncluir(TextBoxIncClas.Text) Then
            MsgBox("Classificação incluida!")
        Else
            MsgBox("Houve um erro ao incluir Classificação!")
        End If
    End Sub

    Private Sub ButtonAltClas_Click(sender As Object, e As EventArgs) Handles ButtonAltClas.Click

        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient
        If ws.ClassificacoesAlterar(TextBoxIdAltClass.Text, TextBoxAltClas.Text) Then
            MsgBox("Classificação alterada!")
        Else
            MsgBox("Houve um erro ao Alterar!")
        End If

    End Sub

    Private Sub ButtonExcClas_Click(sender As Object, e As EventArgs) Handles ButtonExcClas.Click
        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient
        If ws.ClassificacoesExcluir(TextBoxExcClas.Text.ToString) Then
            MsgBox("Classificação Excluida!")
        Else
            MsgBox("Houve um erro ao Excluir!")
        End If
    End Sub

    Private Sub LinkLabelAtuII_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabelAtuII.LinkClicked

        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient

        DataGridView2.DataSource = Nothing
        DataGridView2.DataSource = ws.PessoasListar

    End Sub

    Private Sub ButtonIncPess_Click(sender As Object, e As EventArgs) Handles ButtonIncPess.Click
        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient

        If ws.PessoasIncluir(TextBoxNomePess.Text, TextBoxEmailPess.Text, ComboBoxClassPess.SelectedValue) Then

            MsgBox("Pessoa adicionada com Êxito")
        Else
            MsgBox("Houve um erro ao adicionar a Pessoa!")
        End If


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient

        If ws.PessoasAlterar(TextBoxIdAlt.Text, TextBoxNomAltPess.Text, TextBoxEmailAltPess.Text, ComboBoxAltClass.SelectedValue) Then
            MsgBox("Pessoa Alterada com Êxito")
        Else
            MsgBox("Houve um erro ao Alterar a Pessoa!")

        End If



    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim ws As New ServiceReference1.WS_SOAP_CursoVbNetSoapClient

        If ws.PessoasExcluir(TextBoxNomeExcPess.Text) Then
            MsgBox("Pessoa Excluida com Êxito")
        Else
            MsgBox("Houve um erro ao Excluir a Pessoa!")

        End If


    End Sub


End Class
