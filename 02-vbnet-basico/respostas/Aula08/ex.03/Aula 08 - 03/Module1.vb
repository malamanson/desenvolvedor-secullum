﻿Imports System.Threading

Module Module1

    Sub Main()

        Dim t As New Thread(AddressOf MeuTeste)
        t.Start()
        t.Join()
        Dim y As New Thread(AddressOf MeuTeste2)
        y.Start()
        y.Join()
        Dim w As New Thread(AddressOf MeuTeste3)
        w.Start()
        w.Join()
        Dim j As New Thread(AddressOf MeuTeste4)
        j.Start()
        j.Join()
        Dim z As New Thread(AddressOf MeuTeste5)
        z.Start()
        z.Join()
    End Sub


    Private Sub MeuTeste()
        Dim s As String = ""
        For i = 0 To 9 Step +1
            s = i.ToString
            Thread.Sleep(1000)
            Console.WriteLine(s)

        Next i
        Console.WriteLine("Thread 01")
        Thread.Sleep(1000)
    End Sub

    Private Sub MeuTeste2()
        Dim s As String = ""
        For i = 0 To 9 Step +1
            s = i.ToString
            Thread.Sleep(1000)
            Console.WriteLine(s)

        Next i
        Console.WriteLine("Thread 02")
        Thread.Sleep(1000)
    End Sub

    Private Sub MeuTeste3()
        Dim s As String = ""
        For i = 0 To 9 Step +1
            s = i.ToString
            Thread.Sleep(1000)
            Console.WriteLine(s)

        Next i
        Console.WriteLine("Thread 03")
        Thread.Sleep(1000)
    End Sub

    Private Sub MeuTeste4()
        Dim s As String = ""
        For i = 0 To 9 Step +1
            s = i.ToString
            Thread.Sleep(1000)
            Console.WriteLine(s)

        Next i
        Console.WriteLine("Thread 04")
        Thread.Sleep(1000)
    End Sub

    Private Sub MeuTeste5()
        Dim s As String = ""
        For i = 0 To 9 Step +1
            s = i.ToString
            Thread.Sleep(1000)
            Console.WriteLine(s)

        Next i
        Console.WriteLine("Thread 05")
        Thread.Sleep(1000)
    End Sub


End Module
