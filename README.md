# Desenvolvedor Secullum

Neste repositório você vai encontrar os materiais para aprender o necessário
para se tornar um desenvolvedor na Secullum.

## Instruções

Os materiais estão separados em módulos, numerados pela ordem em que devem ser
estudados (ex: `01-git`, `02-vbnet-basico`, etc). Cada módulo possui as
seguintes pastas:

- `materiais`: Os materiais que devem ser lidos/estudados
- `exercicios`: Exercícios que devem ser realizados

Também fique atento para arquivos `README.md` que podem estar dentro de cada
pasta, pois eles podem ter informações adicionais.

Para iniciar, clique no botão "Fork" que há no canto superior direito no Gitlab:

![Imagem mostrando onde fica o botão Fork](fork.png)

Feito isso, o repositório inteiro será copiado para a sua conta no GitLab.

Para submeter as respostas dos exercícios, crie uma pasta `respostas` dentro do
módulo e faça lá o `commit` e `push` dos arquivos necessários (você aprenderá
sobre como fazer isso no módulo do Git).

**Fique muito atento às datas combinadas para a entrega dos exercícios**, pois o
comprometimento é uma das coisas mais importantes, não só nessa jornada de
aprendizado, mas principalmente ao trabalhar como desenvolvedor.
